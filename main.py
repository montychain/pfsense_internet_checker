import requests
import time
from time import gmtime, strftime
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options

LOGIN = "admin"
PASSWORD = "pfsense"
LOGIN_URL = "https://192.168.167.2/index.php"
RESTART_VPN_URL = "https://192.168.167.2/status_openvpn.php"
SCREEN_SHOT_FILE = "screen_shot.png"
TIMEOUT = 10
TIMEOUT_AFTER_VPN_RESTART = 20

CHECK_URL = "http://178.62.62.81/test.txt"
CHECK_STRING = "3929f225d806c82deccea03a"
CHECK_URL_TIMOUT = 10
CHECKS_LIMIT = 2
TIMEOUT_BETWEEN_CHECKS = 3


class VpnRestartBot(object):

    def __init__(self):
        self.options = Options()
        self.options.headless = True
        self.profile = webdriver.FirefoxProfile()
        self.profile.accept_untrusted_certs = True
        self.driver = webdriver.Firefox(
            firefox_profile=self.profile,
            options=self.options,
            executable_path='./geckodriver')
        self.w = WebDriverWait(self.driver, TIMEOUT)
        self.log_in()
        self.restart_open_vpn()

    def log_in(self):
        self.driver.get(LOGIN_URL)
        self.driver.find_element_by_id("usernamefld").send_keys(LOGIN)
        self.driver.find_element_by_id("passwordfld").send_keys(PASSWORD)
        self.driver.find_element_by_name("login").click()
        self.w.until(EC.presence_of_element_located((By.CLASS_NAME, "fa-sign-out")))

    def restart_open_vpn(self):
        self.driver.get(RESTART_VPN_URL)
        self.w.until(EC.presence_of_element_located((By.CLASS_NAME, "fa-repeat")))
        print(current_time, "Restarting VPN!!! ===========================================")
        self.driver.find_element_by_class_name("fa-repeat").click()
        self.driver.save_screenshot(SCREEN_SHOT_FILE)
        time.sleep(TIMEOUT_AFTER_VPN_RESTART)


def internet_is_alive():
    for i in range(CHECKS_LIMIT):
        try:
            r = requests.get(CHECK_URL, timeout=CHECK_URL_TIMOUT)
            got_string = r.text
            if got_string == CHECK_STRING:
                return True
        except Exception as exc:
            print(current_time, "Failed to get check url", exc)
            time.sleep(TIMEOUT_BETWEEN_CHECKS)
    return False


def keep_internet_alive():
    if not internet_is_alive():
        VpnRestartBot()
    else:
        print(current_time, "Internet works!")
        time.sleep(TIMEOUT_BETWEEN_CHECKS)


if __name__ == '__main__':
    current_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    VpnRestartBot()
    while True:
        current_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        keep_internet_alive()

