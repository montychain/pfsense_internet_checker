## Requirements
- docker
- docker-compose

## QuickStart
Clone the repository from bitbucket
```bash
git clone https://bitbucket.org/montychain/pfsense_internet_checker.git
```
cd to project directory
```bash
cd pfsense_internet_checker
```
Run internet checker by launching it in a container using docker-compose
```bash
docker-compose up --build -d
```