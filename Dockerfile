FROM ubuntu:18.04
RUN apt-get update
RUN apt-get install -y python3-pip
RUN apt-get install -y firefox
COPY ./geckodriver /
COPY ./requirements.txt /
RUN pip3 install -r requirements.txt
COPY ./main.py /
CMD ["python3", "-u", "main.py"]